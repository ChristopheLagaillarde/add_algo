#!/!usr/bin/env python
__program__ = "main.py"
__description__ = "give sum"
__date__ = "07/03/24"
__author__ = "Christophe Lagaillarde"
__version__ = "1.0"
__license__ = "MIT License"
__copyright__ = "Copyright 2024 (c) Christophe Lagaillarde"

import sys
from algo_fun.add import add

def main() -> None: 
	if len(sys.argv) == 1:
		sys.exit("no argument specified")

	sys.argv = sys.argv[1::]

	for i, arg in enumerate(sys.argv):
		sys.argv[i] = int(sys.argv[i])	
	
	add(*sys.argv)	
	
	return None 


if __name__ == "__main__":
	main()

